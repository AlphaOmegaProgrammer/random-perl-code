use strict;
use lib '/home/user/perl/';

use ptest;

my $ptest = ptest->new("John", 69);

$ptest->give_money(100);
$ptest->give_money(150);
$ptest->give_money(1.50);

print $ptest->show();
