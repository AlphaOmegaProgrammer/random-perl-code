package ptest;

use strict;

my $VERSION = v0.0.1;

sub new{
	my $class = shift;
	my $self = bless {
		name => shift,
		age => shift
	}, $class;

	return $self;
}

sub give_money{
	my $self = shift;
	$self->{money} += shift;
}

sub show{
	my $self = shift;
	my $balance = sprintf "\$%.2f", $self->{money};
	return $self->{name}, ', ', $self->{age}, ', ', $balance, "\n";
}
